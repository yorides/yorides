<?php 
class MyCustom404Ctrl extends CI_Controller  {
	public function __construct() {
		parent::__construct();
	}

	public function index(){
        // Make sure you actually have some view file named 404.php
		$data["pagename"]="404";
		$this->load->view("common",$data);
	}
}