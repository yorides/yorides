<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserCont extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library(array("form_validation",'session','upload','pagination'));
		$this->load->model("Admin_model");
		// $this->emp=$this->load->database('emp', TRUE); //if more than one db used
		$this->load->helper(array('form', 'url','security')); 
		define ("SECRETKEY", "mysecretkey1234");

	}
	
	public function UserDashboard(){
		// die;
		if(empty($this->session->userdata('user_auth'))){
			redirect('login');
		}
		$data['url'] = 'user-dashboard';
		
		
		$session_user_values = $this->session->userdata('user_db_values');
		$user_id = $session_user_values[0]['user_id'];
		$data['user_db_values'] = $this->Admin_model->getUserData($user_id);
		$data['who_is_this'] = $data['user_db_values'];
		$this->load->view('common',$data);
	}

	public function AddUser(){
		if(empty($this->session->userdata('admin_auth'))){
			redirect('login');
		}
		if($this->input->post('submit')){
			$this->form_validation->set_rules('username','User Name','required|min_length[3]|max_length[64]');
			$this->form_validation->set_rules('user_email','User Email ID','required|max_length[128]');
			$this->form_validation->set_rules('user_mobile','User Mobile No','required|exact_length[10]');
			$this->form_validation->set_rules('user_pass','User Password','required');
			$this->form_validation->set_rules('sel_prog','Select Programmes','');
			if($this->form_validation->run()==true){
				$insert['username'] = $this->input->post('username');
				$insert['user_email'] = $u_email = $this->input->post('user_email');
				$insert['user_mobile'] = $this->input->post('user_mobile');
				$insert['user_pass'] = openssl_encrypt($this->input->post('user_pass'), 'AES-256-ECB', SECRETKEY);
				$insert['user_assigned_prog'] = json_encode($this->input->post('sel_prog'));
				$check_existence = $this->Admin_model->check_user_existence($u_email);
				if(!empty($check_existence)){
					$this->session->set_flashdata('err_msg','User Already Exist ! Please Login');
				}else{
					$insert_succ = $this->Admin_model->insert_user_data($insert);
					if(!empty($insert_succ)){
						$this->session->set_flashdata('msg','User Details Added Successfully');
						redirect('user-list');
					}else{
						$this->session->set_flashdata('msg','Some technical issue ! Try Again !');
					}
				}
			}
		}
		$data['url'] = 'add-user';
		$data['users_count'] = $this->Admin_model->user_count();
		$data['prog_count'] = $this->Admin_model->prog_count();
		$data['prog_list'] = $this->Admin_model->getProgList();
		$session_user_values = $this->session->userdata('user_db_values');
		$user_id = $session_user_values[0]['user_id'];
		$data['user_db_values'] = $this->Admin_model->getUserData($user_id);
		$data['who_is_this'] = $data['user_db_values'];
		$this->load->view('common',$data);
	}

	public function UserList(){
		if(empty($this->session->userdata('admin_auth'))){
			redirect('login');
		}

		if($this->input->post('del_user_id')){
			$user_id = $this->input->post('del_user_id'); 
			$del_succ = $this->Admin_model->delete_user($user_id);
			if(!empty($del_succ)){

				$this->session->set_flashdata('msg','User Details Deleted Successfully !');
				redirect('user-list');
			}else{
				$this->session->set_flashdata('msg','Some technical issue ! Try Again !');
				redirect('user-list');
			}
		}
		$data['url'] = 'user-list';
		$data['users_count'] = $this->Admin_model->user_count();
		$data['user_list'] = $this->Admin_model->getUserList();
		$data['prog_count'] = $this->Admin_model->prog_count();
		$prog_data = $this->Admin_model->getProgList();
		if(!empty($prog_data)){
			foreach ($prog_data as $key => $value) {
				$temp[$value['progname']] = $value['p_id'];
			}	
			$data['temp'] = $temp;
		}

		$session_user_values = $this->session->userdata('user_db_values');
		$user_id = $session_user_values[0]['user_id'];
		$data['user_db_values'] = $this->Admin_model->getUserData($user_id);
		$data['who_is_this'] = $data['user_db_values'];


		$this->load->view('common',$data);
	}


	public function UserEdit(){
		if(empty($this->session->userdata('admin_auth'))){
			redirect('login');
		}
		if($this->input->post('submit')){

			$this->form_validation->set_rules('username','User Name','required|min_length[3]|max_length[64]');
			// $this->form_validation->set_rules('user_email','User Email ID','required|max_length[128]|valid_email');
			$this->form_validation->set_rules('user_mobile','User Mobile No','required|exact_length[10]|numeric');
			$this->form_validation->set_rules('user_pass','User Password','required');

			if($this->form_validation->run()==true){
				$update['username'] = $this->input->post('username');
				// $update['user_email'] = $u_email = $this->input->post('user_email');
				$update['user_mobile'] = $this->input->post('user_mobile');

				$update['user_pass'] = openssl_encrypt($this->input->post('user_pass'), "AES-256-ECB", SECRETKEY);
				if(!empty($this->input->post('sel_prog'))){
					$update['user_assigned_prog'] = json_encode($this->input->post('sel_prog'));
				}
				$user_id = $this->input->post('hidden_user_id');

				$update_succ = $this->Admin_model->update_user_data($update,$user_id);


				if(!empty($update_succ)){
					$this->session->set_flashdata('msg','User Details Updated Successfully');
					redirect('user-list');
				}else{
					$this->session->set_flashdata('msg','Some technical issue ! Try Again !');
				}

			}
		}

		$user_id = $this->input->get('id'); 
		if(empty($user_id)) {
			redirect('pagenotfound');
		}

		$data['user_data'] = $this->Admin_model->getUserData($user_id);
		if(empty($data['user_data'])){
			redirect('user-list');
		}

		$data['users_count'] = $this->Admin_model->user_count();

		$prog_data = $this->Admin_model->getProgList();
		foreach ($prog_data as $key => $value) {
			$temp[$value['progname']] = $value['p_id'];
		}	
		if(!empty($temp)){
			$data['total_programs'] = $temp;
			$data['db_data'] = json_decode($data['user_data'][0]['user_assigned_prog']);
			$data['user_assigned'] = (array_intersect($data['total_programs'], $data['db_data']));
		}else{
			$this->session->set_flashdata("err_msg",'Add atleast one Programme in order to add User ');
			redirect("add-prog");
		}
		
		
		
		$data['prog_list'] = $this->Admin_model->getProgList();
		$data['prog_count'] = $this->Admin_model->prog_count();
		$session_user_values = $this->session->userdata('user_db_values');
		$user_id = $session_user_values[0]['user_id'];
		$data['user_db_values'] = $this->Admin_model->getUserData($user_id);
		$data['who_is_this'] = $data['user_db_values'];
		$data['url'] = 'edit-user';
		$this->load->view('common',$data);
	}








	public function UserProgList(){
		if(empty($this->session->userdata('user_auth'))){
			redirect('login');
		}
		$user_details =  $this->session->userdata('user_db_values');
		$user_id = json_decode($user_details[0]['user_id']); 
		// $user_id = implode(",", $user_id);
		$user_progs_details = $this->Admin_model->getProgForUser($user_id);
		$progs =  json_decode($user_progs_details[0]['user_assigned_prog']); 

		$prog_id = implode(",", $progs);
		$data['progs_details'] = $this->Admin_model->getProgDetailsForUser($prog_id);
		$data['user_db_values'] = $this->session->userdata('user_db_values');
		$data['prog_count'] = $this->Admin_model->prog_count();
		$data['users_count'] = $this->Admin_model->user_count();
		$session_user_values = $this->session->userdata('user_db_values');
		$user_id = $session_user_values[0]['user_id'];
		$data['user_db_values'] = $this->Admin_model->getUserData($user_id);
		$data['who_is_this'] = $data['user_db_values'];
		$data['url'] = 'user-prog-list';

		$this->load->view('common',$data);
	}

// public function common(){
// 	echo 'common'; die;
// 	$admin_auth = $this->session->userdata('admin_auth');
// 	$user_auth = $this->session->userdata('user_auth');
// 	if(empty($admin_auth)||empty($user_auth)){
// 		redirect('login');
// 	}
// 	$this->load->view('common');
// }


	public function logout(){
		session_destroy();
		redirect('login');
	}


	public function pagenotfound(){
		$this->load->view('pagenotfound');
	}
}
