<?php 
class Admin_model extends CI_Model{

	public function checkUserExists($logindata){
		$data['userid']=$logindata['username'];
		$data['password'] = $logindata['password'];
		$res = $this->db->select("*")->from("tbl_login")->where($data);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function storeSendOtpData($dataArr){
		$res = $this->db->insert("tbl_otp",$dataArr);
		return $res;
	}

	public function checkOtpData($otpReceived,$signInMobile,$userType){
		$res = $this->db->select("*")->from("tbl_otp")->where(array('otp'=>$otpReceived,'mobile'=>$signInMobile,"userType"=>$userType));
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function storeUserData($userData){
		$res = $this->db->insert("tbl_reg_user",$userData);
		// echo $this->db->last_query(); die;
		return $this->db->insert_id();
	}

	public function updateHashId($dbUserId){
		$hashValue['userHashId'] = md5($dbUserId);
		$res = $this->db->where('userId',$dbUserId);
		$res = $this->db->update('tbl_reg_user',$hashValue);
		return $res;
	}


	public function updateTripReqIdAsHashId($tripReqId){
		$hashValue['tripReqHashId'] = md5($tripReqId);
		$res = $this->db->where('tripReqId',$tripReqId);
		$res = $this->db->update('tbl_travaller_trip_req',$hashValue);
		return $hashValue['tripReqHashId'];
	}


	public function getTripIdUsingOrderId($orderId){
		$res = $this->db->select("*")->from("tbl_payment")->where('orderId',$orderId)->get()->result_array();
		return $res;
	}


	public function updateTripHashId($dbGenTripId){
		$hashValue['tripHashId'] = md5($dbGenTripId);
		$res = $this->db->where('tripId',$dbGenTripId);
		$res = $this->db->update('tbl_trip',$hashValue);
		return $hashValue['tripHashId'];
	}

	public function getDriverLatestLoc($driverId){
		$loc['userHashId'] = $driverId;
		$res = $this->db->select("userName,userType,userEmail,signInMobile,userHashId,userAddress,latitude,longitude,allowSignIn")->from("tbl_reg_user")->where($loc);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function editUserData($data_to_update,$userHashId){
		$res = $this->db->where('userHashId',$userHashId);
		$res = $this->db->update('tbl_reg_user',$data_to_update);
		return $res;
	}


	public function getTravellerLatestLoc($travellerId){
		$loc['userHashId'] = $travellerId;
		$res = $this->db->select("userName,userType,userEmail,signInMobile,userHashId,userAddress,latitude,longitude,allowSignIn")->from("tbl_reg_user")->where($loc);
		$res = $this->db->get()->result_array();
		return $res;	
	}

	public function getUserLatestCoordinates($userId){
		$loc['userHashId'] = $userId;
		$res = $this->db->select("latitude,longitude,allowSignIn")->from("tbl_reg_user")->where($loc);
		$res = $this->db->get()->result_array();
		return $res;	
	}


	public function checkUserData($userDetails){
		$res = $this->db->select("*")->from("tbl_reg_user")->where($userDetails);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getJourneyDetails($tripId){
		$res = $this->db->select("*")->from("tbl_travaller_trip_req")->where('tripId',$tripId);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getJourneyDetailsUsingTripReqId($tripReqId){
		$res = $this->db->select("*")->from("tbl_travaller_trip_req")->where('tripReqHashId',$tripReqId);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function deleteTripReqByTravellerUsingTripReqId($tripReqId){

		$res = $this->db->where('tripReqHashId',$tripReqId);
		$res = $this->db->delete('tbl_travaller_trip_req');
		return $res;
	}

	public function storeVehicleData($userData){
		$res = $this->db->insert("tbl_vehicle_reg",$userData);
		return $res;
	}

	public function checkVehicleData($vData){
		$res = $this->db->select("*")->from("tbl_vehicle_reg")->where($vData);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function checkDeviceExist($deviceData){
		$res = $this->db->select("*")->from("tbl_reg_user")->where($deviceData);
		$res = $this->db->get()->result_array();
		return $res;	
	}

	public function checkUserExist($userData){
		$res = $this->db->select("*")->from("tbl_reg_user")->where($userData);
		$res = $this->db->get()->result_array();
		return $res;		
	}

	public function addProfileData($userProfile,$userId){
		$res = $this->db->where('userHashId',$userId);
		$res = $this->db->update('tbl_reg_user',$userProfile);
		return $res;
	}

	public function updateUserLocation($userLocationData,$userId){
		$res = $this->db->where('userHashId',$userId);
		$res = $this->db->update('tbl_reg_user',$userLocationData);
		return $res;
	}

	public function updateBankDetails($userBankDetails,$userId){
		$res = $this->db->where('userHashId',$userId);
		$res = $this->db->update('tbl_reg_user',$userBankDetails);
		return $res;
	}

	public function getActiveDriversList($availableVehicleTypes){
		$res = $this->db->select("*")->from("tbl_vehicle_reg")->where_in('vehicleId',$availableVehicleTypes);
		// ->group_by("vehicleId")->order_by("vehicleId","asc");
		$res = $this->db->get()->result_array();
		// echo $this->db->last_query(); die;

		return $res;
	}

	public function getVehicleIdfromRegisteredVehicles($availableVehicleTypes){
		$res = $this->db->select("*")->from("tbl_vehicle_reg")->where_in('userHashId',$availableVehicleTypes);
		// ->group_by("vehicleId")->order_by("vehicleId","asc");
		$res = $this->db->get()->result_array();
		// echo $this->db->last_query(); die;

		return $res;
	}


	public function getActiveUsersList($driversRegistered){
		$res = $this->db->select("userHashId,latitude,longitude")->from("tbl_reg_user")->where('allowSignIn','1')->where_in('userHashId',$driversRegistered);
		// ->group_by("vehicleId")->order_by("vehicleId","asc");
		$res = $this->db->get()->result_array();
		// echo $this->db->last_query(); die;

		return $res;
	}



	public function getVehicleList(){
		// $res = $this->db->get('tbl_vehicle_config')->result_array();
		// return $res;
		$res = $this->db->select("*")->from("tbl_vehicle_config")->where("isActive","yes");
		$res = $this->db->get()->result_array();
		return $res;

	}

	public function getAllVehicleList(){
		$res = $this->db->get('tbl_vehicle_config')->result_array();
		return $res;
	}

	public function getAllPaymentModeList(){
		$res = $this->db->select("*")->from("tbl_payment_mode_list");
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getPaymentModeList(){
		$res = $this->db->select("*")->from("tbl_payment_mode_list")->where("isActive","yes");
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getPaymentData($pmId){
		$res = $this->db->select("*")->from("tbl_payment_mode_list")->where("pmId",$pmId);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function updatePaymentMode($data_to_update,$pmId){
		$res = $this->db->where('pmId',$pmId);
		$res = $this->db->update('tbl_payment_mode_list',$data_to_update);
		return $res;
	}


	public function getFCMIdsfromDriverAcceptance($tripReqHashId){
		$res = $this->db->select("*")->from("tbl_driveracceptance")->where('tripStatus','requested')->where("tripReqHashId",$tripReqHashId)->order_by('acceptId','asc');
		$res = $this->db->get()->result_array();
		// echo $this->db->last_query(); die;
		return $res;
	}

	public function updatedDriverAcceptanceStatus($tripReqHashId,$driverId){
		$tripStatus['tripStatus'] ='rejected';
		$res = $this->db->where('tripReqHashId',$tripReqHashId);
		$res = $this->db->where('driverId',$driverId);
		$res = $this->db->update('tbl_driveracceptance',$tripStatus);
		return $res;
	}


	public function getPaymentModeDetails($pmId){
		$res = $this->db->select("paymentMode")->from("tbl_payment_mode_list")->where("pmId",$pmId);
		$res = $this->db->get()->result_array();
		return $res;
	}



	public function getVehicleConfig($vehicleId){
		$res = $this->db->select("*")->from("tbl_vehicle_config")->where("vehicleId",$vehicleId);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getVehicleDetails($vehicleId){
		$res = $this->db->select("*")->from("tbl_vehicle_reg")->where("vehicleId",$vehicleId);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function insertTripRequestRecords($dataInsert){
		$res = $this->db->insert("tbl_driveracceptance",$dataInsert);
		return $res; 
	}



	public function getVehicleDetailsUsingDriverId($driverId){
		$a = $this->db->select("vehicleId")->from("tbl_vehicle_reg")->where("userHashId",$driverId)->get()->result_array();
		$b = $this->db->select("vType")->from("tbl_vehicle_config")->where("vehicleId",$a[0]['vehicleId'])->get()->result_array();
		return $b;
	}


	public function getFilteredVehicleList($availableDriversList){
		$res = $this->db->select("*")->from("tbl_vehicle_config")->where_in("vehicleId",$availableDriversList);
		$res = $this->db->get()->result_array();
		return $res;
	}


	public function selectedVehicleDriverList($vehicleId){
		$res = $this->db->select("*")->from("tbl_vehicle_reg")->where("vehicleId",$vehicleId);
		$res = $this->db->get()->result_array();	
		return $res;
	}


	public function getFCMIds($listOfDriversId){
		$res = $this->db->select("fcmToken")->from("tbl_reg_user")->where_in("userHashId",$listOfDriversId);
		$res = $this->db->get()->result_array();
		return $res;
	}

	// public function getOnlyAllowSignInUsers($listOfDriversId){
	// 	$res = $this->db->select("fcmToken")->from("tbl_reg_user")->where_in("userHashId",$listOfDriversId);
	// 	$res = $this->db->get()->result_array();
	// 	return $res;
	// }



	public function getDriversListUsingUserId($userIdForDriversList){
		$res=$this->db->select("*")->from("tbl_reg_user")->where_in("userHashId",$userIdForDriversList)->order_by('userId','asc');
		$res=$this->db->get()->result_array();
		return $res;
	}

	public function getCurrentlyAvailbleDrivers($eachDriverIdValue){
		// $res=$this->db->select("driverId")->from("tbl_travaller_trip_req")->where_in("driverId",$eachDriverIdValue)->where_in("driverId",array("cancelled","cancell"));
		// $res=$this->db->get()->result_array();
		// return $res;

		$this->db->select_max('tripReqId');
		$this->db->from('tbl_travaller_trip_req')->where("driverId",$eachDriverIdValue)->where_in('tripStatus',array('cancelledByTraveller','cancelledByDriver','completed','rejected'));
		$query = $this->db->get();
		$r=$query->result();
		return $r;
	}





	public function updateDeviceDetails($deviceData,$userId){
		$res = $this->db->where('userHashId',$userId);
		$res = $this->db->update('tbl_reg_user',$deviceData);
		return $res;
	}

	public function updateUserDetails($detailsData,$signInMobile){
		$res = $this->db->where('signInMobile',$signInMobile);
		$res = $this->db->update('tbl_reg_user',$detailsData);
		return $res;
	}

	public function createTrip($tripContent,$tripReqId){
		// $res = $this->db->insert("tbl_trip",$tripContent);
		// return $this->db->insert_id(); 

		$res = $this->db->where('tripReqHashId',$tripReqId);
		$res = $this->db->update('tbl_travaller_trip_req',$tripContent);
		return $res;
	}

	public function storeTravellerReqData($travellerTripReqData){
		$res = $this->db->insert("tbl_travaller_trip_req",$travellerTripReqData);
		return $this->db->insert_id(); 
	}

	public function storePaymentData($paymentDetails){
		$res = $this->db->insert("tbl_payment",$paymentDetails);
		return $res; 
	}


	public function checkTripAcceptedOrNot($travellerId,$tripReqId){
		$res = $this->db->select("*")->from("tbl_travaller_trip_req")->where("userId",$travellerId)->where('tripStatus','requested')->where("tripReqHashId",$tripReqId);
		$res = $this->db->get()->result_array();
		return $res; 
	}

	// *******************************************************************************
		// UI
	
	public function checkVTypeInDB($data_to_insert){
		$res = $this->db->select('*')->from('tbl_vehicle_config')->where('vType',$data_to_insert['vType']);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function storeVehiclePricingData($data_to_insert){
		$res = $this->db->insert("tbl_vehicle_config",$data_to_insert);
		return $res;
	}
	public function storeFcmLogForTravellerConfirmTrip($sendFCMVal){
		$data_to_insert['fcmLog'] = $sendFCMVal;
		$res = $this->db->insert("tbl_fcm_log",$data_to_insert);
		return $res;
	}
	public function getDriverInfoForTrip($tripId,$travellerId){
		$res = $this->db->select('*')->from('tbl_travaller_trip_req')->where('tripId',$tripId)->where('userId',$travellerId);
		$res = $this->db->get()->result_array();
		// echo $this->db->last_query();die;
		return $res;
	}

	public function getTravellerInfoForTrip($tripId,$driverId){
		$res = $this->db->select('*')->from('tbl_travaller_trip_req')->where('tripId',$tripId)->where('driverId',$driverId);
		$res = $this->db->get()->result_array();
		// echo $this->db->last_query();die;
		return $res;
	}

	public function getTripDetails($tripId){
		$res = $this->db->select('*')->from('tbl_travaller_trip_req')->where('tripId',$tripId);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function checkTripIsReqOrNot($travellerId,$tripId){
		$res =$this->db->select('*')->from('tbl_travaller_trip_req')->where('userId',$travellerId)->where('tripId',$tripId);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function checkTripIsReqOrNotUsingDriverId($driverId,$tripId){
		$res =$this->db->select('*')->from('tbl_travaller_trip_req')->where('driverId',$driverId)->where('tripId',$tripId);
		$res = $this->db->get()->result_array();
		return $res;
	}



	public function checkTripIsReqOrNotUsingTripReqId($travellerId,$tripReqId){
		$res=$this->db->select('*')->from('tbl_travaller_trip_req')->where('userId',$travellerId)->where('tripReqHashId',$tripReqId);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function checkTripIsExistOrNot($travellerId,$tripId){
		$res =$this->db->select('*')->from('tbl_trip')->where('travellerId',$travellerId)->where('tripHashId',$tripId);
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function checkAnyDriverConfirmedTrip($travellerId){
		$res = $this->db->select('*')->from('tbl_travaller_trip_req')->where('userId',$travellerId)->where('tripStatus',"accepted");
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getTripStatusUsingTravellerId($travellerId){
		$res = $this->db->select('*')->from('tbl_travaller_trip_req')->where('userId',$travellerId)->where('tripStatus !=','completed')->where('tripStatus !=','cancelledByTraveller')->where('tripStatus !=','cancelledByDriver')->order_by('tripReqId','desc');
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getTripStatusUsingDriverId($driverId){
		$res = $this->db->select('*')->from('tbl_travaller_trip_req')->where('driverId',$driverId)->where('tripStatus !=','completed')->where('tripStatus !=','cancelledByTraveller')->where('tripStatus !=','cancelledByDriver')->order_by('tripReqId','desc');
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getTripHistoryUsingDriverId($driverId,$tripMode){
		$res = $this->db->select("*")->from("tbl_travaller_trip_req")->where("driverId",$driverId)->where_in("tripStatus",$tripMode)->order_by('tripReqId','desc');
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function getTripHistoryUsingTravellerId($travellerId,$tripMode){
		$res = $this->db->select("*")->from("tbl_travaller_trip_req")->where("userId",$travellerId)->where_in("tripStatus",$tripMode)->order_by('tripReqId','desc');
		$res = $this->db->get()->result_array();
		return $res;
	}


	public function checkPreviousTripPaymentType($travellerId){
		$res = $this->db->select('*')->from('tbl_travaller_trip_req')->where('userId',$travellerId)->order_by('tripReqId',"desc");
		$res = $this->db->get()->result_array();
		return $res;
	}


	public function updateTrip($tripId,$driverTripLoc){
		// $res = $this->db->where('tripHashId',$tripId)->where('tripStatus','accepted');
		$res = $this->db->where('tripId',$tripId);
		$res = $this->db->update('tbl_travaller_trip_req',$driverTripLoc);
		// echo $this->db->last_query();die;
		return $res;
	}

	public function getVehicleDetailsForAmountCalc($tripId){
		// $q = "SELECT * FROM tbl_vehicle_config WHERE vehicleId = (SELECT vehicleId FROM tbl_vehicle_reg WHERE userHashId = (SELECT driverId FROM tbl_trip WHERE tripHashId = '$tripId'))";

		$a = $this->db->select('driverId')->from('tbl_travaller_trip_req')->where('tripId',$tripId)->get()->result_array();
		$b = $this->db->select('vehicleId')->from('tbl_vehicle_reg')->where('userHashId',$a[0]['driverId'])->get()->result_array();
		$c = $this->db->select('pricePerKM')->from('tbl_vehicle_config')->where('vehicleId',$b[0]['vehicleId'])->get()->result_array();
		// echo $this->db->last_query();die;
		return $c;
	}


	public function getAllTripsList(){
		$res = $this->db->get('tbl_travaller_trip_req')->result_array();
		return $res;
	}



}
?>