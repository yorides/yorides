		<script type="text/javascript">

			function delrecord(id){
				var yes = confirm("Are you Sure ?");
				if(yes){
					frmList.deleteid.value=id;
					frmList.submit();
					return true;
				}else{
					frmList.deleteid.value=null;
					return false;
				}
			}
		</script>
		<section class="content">
			<div class="container-fluid">
				<!-- Exportable Table -->
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="header">
								<!-- <div class="align-right"><a type="button" class="btn btn-warning btn-sm" href="<?php echo base_url().'admin/addVehiclePricing' ?>">Add New Vehicle</a></div> -->
								<h2>
									<b>Payment Modes</b>
								</h2>
								<div class="text-center" style="padding-bottom:10px" id="err_hide">
									<span class="errStyle"><?php echo $this->session->flashdata('Succ'); ?></span >
									<span class="errStyle1"><?php echo $this->session->flashdata('DelSucc'); ?></span >
								</div>  
							</div>
							<form method="post" name="frmList" id="frmList">
								
								<div class="body">
									<div class="table-responsive">
										<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
											<thead>
												<tr>
													<th width="4%">Sl.No</th>
													<th width="50%">Payment Mode</th>
													<th width="20%">Status</th>
													<th width="10%">Action</th>
												</tr>
											</thead>
											<tbody>
												<?php $i = 0; foreach ($dbPaymentModesData as $value) {
													$i++;	?>
													<tr>
														<td><?php echo $i; ?></td>
														<td><?php echo ucwords($value['paymentMode']); ?></td>
														<td><?php 
														
														if ($value['isActive']=='yes'){ ?>
															<button type='button' class='btn btn-xs btn-success'>Active</button>
														<?php }else{ ?>
														
															<button type='button' class='btn btn-xs btn-danger'>Inactive</button>
															<?php } ?>

														</td>
														<td><input type="hidden" name="pmId" id="pmId" value="<?php echo $value['pmId'];?>">
															<a class="btn btn-xs btn-warning" title="Edit" href="<?php echo base_url().'admin/editPaymentDashboard?pmId='.($value["pmId"]); ?>"> 
																Edit
															</a>
															&nbsp;
															&nbsp;
														</a>
													</td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- #END# Exportable Table -->
			</div>
		</section>
