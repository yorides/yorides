		<script type="text/javascript">

			function delrecord(id){
				var yes = confirm("Are you Sure ?");
				if(yes){
					frmList.deleteid.value=id;
					frmList.submit();
					return true;
				}else{
					frmList.deleteid.value=null;
					return false;
				}
			}
		</script>
		<section class="content">
			<div class="container-fluid">
				<!-- Exportable Table -->
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="header">
								<h2>
									<b>Registered Drivers Details</b>
								</h2>
								<div class="text-center" style="padding-bottom:10px" id="err_hide">
									<span class="errStyle"><?php echo $this->session->flashdata('Succ'); ?></span >
									<span class="errStyle1"><?php echo $this->session->flashdata('DelSucc'); ?></span >
								</div>  
							</div>
							<form method="post" name="frmList" id="frmList">
								
								<div class="body">
									<div class="table-responsive">
										<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
											<thead>
												<tr>
													<th width="1%">Sl.No</th>
													<th width="20%">Name</th>
													<th width="10%">Mobile</th>
													<th width="5%">Type</th>
													<th width="10%">Reg No</th>
													<th width="5%">Status</th>
													<th width="5%">Bank</th>
													<th width="5%">Lic</th>
													<th width="5%">Reg</th>
													<th width="5%">Image</th>
													<th width="5%">Loc</th>
													<th width="10%">Action</th>
												</tr>
											</thead>
											<tbody>
												<?php $i = 0; foreach ($dbUserData as $value) {
													// die;
													$i++;	?>
													<tr style='font-size:12px !important'>
														<td><?php echo $i; ?></td>
														<td><?php echo $value['userName']; ?></td>
														<td><?php echo $value['signInMobile']; ?></td>
														<td style='font-size:9.5px;font-weight:bold !important'><?php echo $vList[$value['vehicleId']]??null; ?></td>
														<td><?php echo $value['vehicleRegNo']; ?></td>
														<td>
															<?php if($value['allowSignIn']==1){ ?>
															<a style="font-size:10px !important" href="#" class="btn btn-success btn-xs">Active</a>
															<?php } elseif($value['allowSignIn']==0){ ?>
															<a style="font-size:10px !important" href="#" class="btn btn-danger btn-xs">Inactive</a>
															<?php } ?>
														</td>
														<td style='font-size:10px !important'>
														<?php echo '<b>Bank:</b>'.$value['bankName'].'<br><b>Acc No:</b>'.$value['bankAccountNo'].'<br><b>IFSC:</b>'.$value['bankIfsc'].'</br><b>Name:</b>'.$value['accHolderName']; ?>
														</td>
														<td>
														<a style="font-size:10px !important" target="_blank" href="<?php echo base_url().'/uploads/licCopy/'.$value['userHashId'].'.png' ?> " class="btn btn-info btn-xs"><i style="font-size:12px !important" class="material-icons">file_download</i></a>
														</td>
														<td>
														<a style="font-size:10px !important" target="_blank" href="<?php echo base_url().'/uploads/regCopyBack/'.$value['userHashId'].'.png' ?> " class="btn btn-info btn-xs"><i style="font-size:12px !important" class="material-icons">file_download</i></a>
														</td>
														<td>
														<a style="font-size:10px !important" target="_blank" href="<?php echo base_url().'/uploads/userImage/'.$value['userHashId'].'.png' ?> " class="btn btn-info btn-xs"><i style="font-size:12px !important" class="material-icons">file_download</i></a>
														</td>
														<td style="font-size:10px !important">
															<a style="font-size:10px !important" target="_blank" href ="https://maps.google.com/?q=<?php echo $value['latitude'] ?>,<?php echo $value['longitude'] ?>"  class="btn btn-xs btn-primary"><i style="font-size:12px !important" class="material-icons">directions</i></a>
														</td>
														<td><input type="hidden" name="userHashId" id="userHashId" value="<?php echo $value['userHashId'];?>">
															<a class="btn btn-xs btn-warning" style="font-size:12px !important" title="Edit" href="<?php echo base_url().'admin/editUser?userHashId='.($value["userHashId"]); ?>" target='_blank'> 
																Edit
															</a>
															&nbsp;
															&nbsp;
														</td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- #END# Exportable Table -->
				</div>
			</section>
