<section class="content">
	<div class="container-fluid">
		<div class="text-center" style="padding-bottom:10px" id="err_hide">
			<span class="errStyle"><?php echo $this->session->flashdata('Succ'); ?></span >
		</div>  
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<b>Edit User Status</b>
						</h2>
					</div>
					<div class="body">
						<form method="post" name="frmVehiclePricing" id="frmVehiclePricing" enctype="multipart/form-data">

							<label>User Name</label>
							<div class="form-group">
								<div class="form-line">
									<input type="text"   id="vehicleName" value="<?php echo $userData[0]['userName'] ?? null; ?>" class="form-control" placeholder="Enter Name" disabled>
								</div>
							</div>

							<label>Status</label>
							<div class="form-group">
								<div class="form-line">
									<select class="form-control show-tick" name="allowSignIn" id="allowSignIn" required="true" >
										<option value="">-- Please select --</option>
										<option value="1" <?php echo (!empty($userData[0]['allowSignIn']==1)?'selected':null) ?>>Active</option>
										<option value="0" <?php echo (!empty($userData[0]['allowSignIn']==0)?'selected':null) ?>>Inactive</option>
									</select>
								</div>
							</div>

							<input type="submit" name="submit" class="btn btn-success m-t-15 waves-effect" value="Submit">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
