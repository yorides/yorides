<section class="content">
	<div class="container-fluid">
		<div class="text-center" style="padding-bottom:10px" id="err_hide">
			<span class="errStyle"><?php echo $this->session->flashdata('Succ'); ?></span >
		</div>  
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<div class="align-right"><a type="button" class="btn btn-warning btn-sm" href="<?php echo base_url().'admin/paymentDashboard' ?>">Payment Modes List</a></div>
						<h2>
							<b>Edit Payment Modes</b>
						</h2>
					</div>
					<div class="body">
						<form method="post" name="frmVehiclePricing" id="frmVehiclePricing" enctype="multipart/form-data">
							<label>Payment Mode Name</label>
							<div class="form-group">
								<div class="form-line">
									<input type="text"  value="<?php echo $paymentData[0]['paymentMode'] ?? null; ?>" class="form-control" disabled>
								</div>
							</div>
							

							<label>Status</label>
							<div class="form-group">
								<div class="form-line">
									<select class="form-control show-tick" name="isActive" id="isActive" required="true" >
										<option value="">-- Please select --</option>
										<option value="yes" <?php echo (!empty($paymentData[0]['isActive']=='yes')?'selected':null) ?>>Active</option>
										<option value="no" <?php echo (!empty($paymentData[0]['isActive']=='no')?'selected':null) ?>>Inactive</option>
									</select>
								</div>
							</div>

							<input type="submit" name="submit" class="btn btn-success m-t-15 waves-effect" value="Submit">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
