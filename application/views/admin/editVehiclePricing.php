<section class="content">
	<div class="container-fluid">
		<div class="text-center" style="padding-bottom:10px" id="err_hide">
			<span class="errStyle"><?php echo $this->session->flashdata('Succ'); ?></span >
		</div>  
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<div class="align-right"><a type="button" class="btn btn-warning btn-sm" href="<?php echo base_url().'admin/vehiclePricingList' ?>">Vehicle Pricing List</a></div>
						<h2>
							<b>Edit Vehicle and Pricing</b>
						</h2>
					</div>
					<div class="body">
						<form method="post" name="frmVehiclePricing" id="frmVehiclePricing" enctype="multipart/form-data">
							<label>Vehicle Name</label>
							<div class="form-group">
								<div class="form-line">
									<input type="text"   id="vehicleName" value="<?php echo $vehicleData[0]['vType'] ?? null; ?>" class="form-control" placeholder="Enter Name" disabled>
								</div>
							</div>
							<label>Vehicle Price Per KM</label>
							<div class="form-group">
								<div class="form-line">
									<input type="number"  name="vehiclePricePerKM"  id="vehiclePricePerKM" value="<?php echo $vehicleData[0]['pricePerKM'] ?? null; ?>" class="form-control" placeholder="Enter Price Per KM" >
								</div>
							</div>
							<label>Vehicle Surcharges</label>
							<div class="form-group">
								<div class="form-line">
									<input type="number"  name="vehicleSurcharges"  id="vehicleSurcharges" value="<?php echo $vehicleData[0]['surCharge'] ?? null; ?>" class="form-control" placeholder="Enter Price Per KM" >
								</div>
							</div>

							<label>SGST</label>
							<div class="form-group">
								<div class="form-line">
									<input type="number"  name="SGST"  id="SGST" value="<?php echo $vehicleData[0]['SGST'] ?? null; ?>" class="form-control" placeholder="Enter SGST" >
								</div>
							</div>

							<label>CGST</label>
							<div class="form-group">
								<div class="form-line">
									<input type="number"  name="CGST"  id="CGST" value="<?php echo $vehicleData[0]['CGST'] ?? null; ?>" class="form-control" placeholder="Enter CGST" >
								</div>
							</div>

							<label>Base Fare</label>
							<div class="form-group">
								<div class="form-line">
									<input type="number"  name="baseFare"  id="baseFare" value="<?php echo $vehicleData[0]['baseFare'] ?? null; ?>" class="form-control" placeholder="Enter Base Fare" >
								</div>
							</div>

							<label>Hourly charges</label>
							<div class="form-group">
								<div class="form-line">
									<input type="number"  name="hourlyCharge"  id="hourlyCharge" value="<?php echo $vehicleData[0]['hourlyCharge'] ?? null; ?>" class="form-control" placeholder="Enter Hourly Charge" >
								</div>
							</div>

							<label>Status</label>
							<div class="form-group">
								<div class="form-line">
									<select class="form-control show-tick" name="isActive" id="isActive" required="true" >
										<option value="">-- Please select --</option>
										<option value="yes" <?php echo (!empty($vehicleData[0]['isActive']=='yes')?'selected':null) ?>>Active</option>
										<option value="no" <?php echo (!empty($vehicleData[0]['isActive']=='no')?'selected':null) ?>>Inactive</option>
									</select>

									<!-- 	<input type="text"  name="isActive"  id="isActive" value="<?php echo $vehicleData[0]['isActive'] ?? null; ?>" class="form-control" placeholder="Enter Hourly Charge" > -->
								</div>
							</div>

							<input type="submit" name="submit" class="btn btn-success m-t-15 waves-effect" value="Submit">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
